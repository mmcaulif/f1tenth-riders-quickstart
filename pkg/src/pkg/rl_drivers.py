import gym
from stable_baselines3 import PPO

model = PPO.load("ppo-08-04-2021-04-06-06-PB")

def convert_range(value, input_range, output_range):
    (in_min, in_max), (out_min, out_max) = input_range, output_range
    in_range = in_max - in_min
    out_range = out_max - out_min
    return (((value - in_min) * out_range) / in_range) + out_min

class PPODriver:
    def process_lidar(self, ranges):
        s_min = -0.4189
        s_max = 0.4189
        v_min = -5
        v_max = 20
        ranges = convert_range(ranges, [0, 30], [-1, 1])
        action, _ = model.predict(ranges)
        steering_angle = convert_range(action[0], [-1, 1], [s_min, s_max])
        speed = convert_range(action[1], [-1, 1], [v_min, v_max])
        return speed, steering_angle
